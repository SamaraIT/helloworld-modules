# Playing with modules
- based on https://developer.ibm.com/tutorials/java-modularity-4/
- https://github.com/mohamed-taman/Java-SE-Code-Examples

$ java -p out/production/ -m hr.samara.play.client/hr.samara.play.main.Client

## Unnamed module
- The unnamed module implicitly exports all the package’s types and reads all other modules 
- There is no way to refer to it in a requires directive from a named module, so a named module cannot depend on the unnamed module.

### Compile and run Java 8 app
$ cd unnamed  

$ javac -d target/unnamed src/play/UnnamedModuleApp.java  
$ java -cp target/unnamed play.UnnamedModuleApp  

### Analyze class
$ jdeps target/unnamed/play/UnnamedModuleApp.class  
```
UnnamedModuleApp.class -> java.base
   play                                               -> java.io                                            java.base
   play                                               -> java.lang                                          java.base
   play                                               -> java.lang.module                                   java.base
   play                                               -> java.util                                          java.base
```
$ jdeps -s target/unnamed/play/UnnamedModuleApp.class  
```
UnnamedModuleApp.class -> java.base  
```
$ jdeps --list-deps target/unnamed/play/UnnamedModuleApp.class  
```
   java.base
```

$ javac -d target/unnamed src/play/UnnamedModule2App.java  
$ java -cp target/unnamed play.UnnamedModule2App  

#### jdeps - more examples
$ jdeps target/unnamed/play/UnnamedModule2App.class  
```
UnnamedModule2App.class -> java.base  
UnnamedModule2App.class -> java.logging 
   play                                               -> java.lang                                          java.base
   play                                               -> java.util.logging                                  java.logging
```

$ jdeps -s target/unnamed/play/UnnamedModule2App.class  
```
UnnamedModule2App.class -> java.base  
UnnamedModule2App.class -> java.logging  
```

$ jdeps --list-deps target/unnamed/play/UnnamedModule2App.class  
```
   java.base
   java.logging
```

### Package and -describe-module
$ mkdir mlib

$ jar -cfv `mlib/unnamed.module.test.jar` -C target/unnamed .

$ java -cp mlib/unnamed.module.test.jar play.UnnamedModuleApp

$ jar --file mlib/unnamed.module.test.jar --describe-module  
```
No module descriptor found. Derived automatic module.

unnamed.module.test automatic
requires java.base mandated
contains play
```

# Services in a Modular Application
## Developing Service module
### Compile interface
$ javac demo.spi/src/module-info.java demo.spi/src/demo/spi/Greeter.java  
### Package JAR
$ jar --create --file mods/service.jar -C demo.spi/src .  


## Developing Service Provider module
$ javac -p mods/service.jar demo.provider/src/module-info.java demo.provider/src/demo/service/impl/GreeterImpl.java  
$ jar --create --file mods/provider.jar -C demo.provider/src .  

## Developing Service Provider module using provider method
javac -p mods/service.jar demo.provider2/src/module-info.java demo.provider2/src/demo/service/two/GreeterProvider.java  
jar --create --file mods/provider2.jar -C demo.provider2/src .

## Developing Service client application
$ javac -p mods/service.jar demo.client/src/module-info.java demo.client/src/demo/client/ClientDemo.java  
$ jar --create --file mods/client.jar -C demo.client/src .  
$ java -p mods -m demo.client/demo.client.ClientDemo  
  
  or  

$ jar --create --file mods/client.jar --main-class demo.client.ClientDemo  -C demo.client/src .  
$ java -p mods -m demo.client  
```
ServiceLoader.load(Greeter.class) = java.util.ServiceLoader[demo.spi.Greeter]

peek in type= interface demo.spi.Greeter
Processing provider: class demo.service.two.GreeterProvider$1
Hi from GreeterProvider - using provider method!
peek in type= class demo.service.impl.GreeterImpl
Processing provider: class demo.service.impl.GreeterImpl
Hello from  GreeterImpl
```

### show-module-resolution
$ java --show-module-resolution -p mods -m demo.client/demo.client.ClientDemo

### describe module
$ jar -d -f mods/provider2.jar  
```
demo.provider.two jar:file:///D:/workspaces/playground/helloworld-modules-master/mods/provider2.jar/!module-info.class
requires demo.spi
requires java.base mandated
provides demo.spi.Greeter with demo.service.two.GreeterProvider
contains demo.service.two
```
Provider depends on demo.spi service module.  

$ jar --describe-module --file=mods/client.jar  
```
demo.client jar:file:///D:/workspaces/playground/helloworld-modules-master/mods/client.jar/!module-info.class
requires demo.spi
requires java.base mandated
uses demo.spi.Greeter
contains demo.client
main-class demo.client.ClientDemo
```
Client depends on java.base and demo.spi service module.  
The client module does not depend on provider module and it is not aware of it at compile time.  

## Ref
http://java.boot.by/ocpjd11-upgrade-guide/ch02.html


# Migration to a Modular Application

## Java 8 application
### info-logger - library
$ mkdir -p target/logger  
$ javac -d target/logger info-logger/src/by/iba/logging/InfoLogger.java

$ mkdir lib  
$ jar -cvf lib/info-logger.jar -C target/logger .

### info-app - application
$ mkdir -p target/app  
$ javac -d target/app -cp lib/info-logger.jar info-app/src/by/iba/app/App.java  
$ jar -cvf lib/info-app.jar -C target/app .

#### run - Java 8 way
$ java -classpath ./lib/info-logger.jar;./lib/info-app.jar by.iba.app.App  

#### run as automatic modules - Java 9 - before migration
$ java --add-modules info.logger --module-path lib/info-logger.jar;lib/info-app.jar by.iba.app.App

## Bottom up migration
- make library jar modular
- app jar which is not migrated can be run on classpath or on module path

### check dependencies and create module.info.java file
$ jdeps -summary lib/info-logger.jar
```
info.logger -> java.base
info.logger -> java.logging
```  
$ jdeps --generate-module-info . lib/info-logger.jar    
$ mv info.logger/module-info.java info-logger/src  
* module-info.java:  
```
module info.logger {
    requires transitive java.logging;
    exports by.iba.logging;
}
```

### extra - check app.jar dependencies - before migration using classpath
$ jdeps -cp lib/info-logger.jar -s lib/info-app.jar
```
info-app.jar -> lib\info-logger.jar
info-app.jar -> java.base
info-app.jar -> java.logging
```
you have to add info-logger.jar, without it `info-app.jar -> not found`  
$ jdeps -s lib/info-app.jar  
```
info-app.jar -> java.base
info-app.jar -> java.logging
info-app.jar -> not found
```

### generate dependency graph
$ jdeps -cp lib/info-logger.jar  `--dot-output` . lib/*.jar  
produces 3 files:
1. info-app.jar.dot
2. info-logger.jar.dot
3. summary.dot

#### summary.dot
```
digraph "summary" {
  "info.logger"                                      -> "java.base (java.base)";
  "info.logger"                                      -> "java.logging (java.logging)";
  "info-app.jar"                                     -> "info.logger";
  "info-app.jar"                                     -> "java.base (java.base)";
  "info-app.jar"                                     -> "java.logging (java.logging)";
}
```
You can visualize module dependency graph (e.g. at http://www.webgraphviz.com/):

### compile and package modular jar
$ javac -d target/logger info-logger/src/by/iba/logging/InfoLogger.java info-logger/src/module-info.java  
$ jar -cvf lib/info-logger.jar -C target/logger .  
### describe modular jar
$ jar -f lib/info-logger.jar -d  
```
info.logger jar:file:///D:/git/HelloWorld-Modules/lib/info-logger.jar/!module-info.class
exports by.iba.logging
requires java.base mandated
requires java.logging transitive
```

$ jar -f lib/info-app.jar -d
```
No module descriptor found. Derived automatic module.

info.app automatic
requires java.base mandated
contains by.iba.app
```

#### Jar options
| short | full | description |
| ------ | ------ | ------ |
| -c | --create  | Create the archive
| -f | --file=FILE  | The archive file name.
| -d | --describe-module  | Print the module descriptor, or automatic module name
| -C DIR | |Change to the specified directory and include the following file (sets path to compiled code)
| -e | --main-class=CLASSNAME | The application entry point for stand-alone applications bundled into a modular, or executable, jar archive

### run semi migrated application
#### run top/app jar as `unnamed module` 
$ java `--add-modules` info.logger --module-path lib/info-logger.jar --class-path lib/info-app.jar by.iba.app.App  
$ java --limit-modules java.base,java.logging --show-module-resolution --add-modules info.logger -p lib/info-logger.jar -cp lib/info-app.jar by.iba.app.App
```
root java.logging jrt:/java.logging
root info.logger file:///D:/git/HelloWorld-Modules/lib/info-logger.jar
root java.base jrt:/java.base
info.logger requires java.logging jrt:/java.logging
java.base binds java.logging jrt:/java.logging
Mar 01, 2020 11:48:35 AM by.iba.logging.InfoLogger log
INFO: Application started ...
Mar 01, 2020 11:48:35 AM by.iba.app.App main
INFO: Application finished.
```

#### run top/app jar as `automatic module` 
$ java `--add-modules` info.logger -p lib/info-logger.jar;lib/info-app.jar -m info.app/by.iba.app.App  
$ java --limit-modules java.base,java.logging --show-module-resolution --add-modules info.logger -p lib/info-logger.jar;lib/info-app.jar -m info.app/by.iba.app.App
```
root info.app file:///D:/git/HelloWorld-Modules/lib/info-app.jar automatic
root info.logger file:///D:/git/HelloWorld-Modules/lib/info-logger.jar
info.logger requires java.logging jrt:/java.logging
java.base binds java.logging jrt:/java.logging
Mar 01, 2020 11:55:59 AM by.iba.logging.InfoLogger log
INFO: Application started ...
Mar 01, 2020 11:55:59 AM by.iba.app.App main
INFO: Application finished.
```

### extra - check app.jar dependencies - after library migration using module-path
$ jdeps `--module-path` lib/info-logger.jar -s lib/info-app.jar
```
info-app.jar -> info.logger
info-app.jar -> java.base
info-app.jar -> java.logging
```

## Ref
- http://java.boot.by/ocpjd11-upgrade-guide/ch06.html
- Java SE: Exploiting Modularity and Other New Features Ed 1.1
