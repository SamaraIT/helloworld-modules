package demo.client;

import demo.spi.Greeter;

import java.util.ServiceLoader;

public class ClientDemo {

  public static void main(String[] args) {
    ServiceLoader<Greeter> services = ServiceLoader.load(Greeter.class);
    System.out.println("ServiceLoader.load(Greeter.class) = " + services + "\n");

//    services.forEach(it -> {
//      System.out.println("Processing provider: " + it.getClass());
//      it.greet();
//    });

    ServiceLoader.load(Greeter.class)
      .stream()
      .peek(it -> System.out.println("peek in type= " + it.type()))
      .map(ServiceLoader.Provider::get)
      .forEach((it -> {
        System.out.println("Processing provider: " + it.getClass());
        it.greet();
      }
      ));
  }
}
