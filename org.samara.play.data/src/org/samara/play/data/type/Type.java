package org.samara.play.data.type;

public enum Type {
  XML,
  JSON,
  STRING
}
