import demo.service.two.GreeterProvider;

module demo.provider.two {
  requires demo.spi;
  provides demo.spi.Greeter with GreeterProvider;
}
