package demo.service.two;

import demo.spi.Greeter;

public class GreeterProvider {

  /**
   * If the service provider declares a provider() method, then the service loader invokes that method to obtain an instance of the service provider.
   * A provider method is a public static method named "provider" with no formal parameters and a return type that is assignable to the service's interface or class.
   * In this case, the service provider itself NEED NOT be assignable to the service's interface or class.
   */
  public static Greeter provider() {
    return new Greeter() {
      @Override
      public void greet() {
        System.out.println("Hi from GreeterProvider - using provider method!");
      }
    };
  }
}
