package hr.samara.play.main;

import hr.samara.play.support.RendererSupport;
import org.samara.play.data.Message;
//import org.samara.play.data.type.Type; // Package 'org.samara.play.data.type' is declared in module 'org.samara.play.data', which does not export it to module 'hr.samara.play.client'

public class Client {

  public static void main(String[] args) {

    RendererSupport support = new RendererSupport();
    support.render("Welcome to the Java 12 Platform Module System");
    Message msg = support.getCurrentMessage();

    System.out.printf("%n %s %n %s", "-----------------", msg.getMessage());
    System.out.printf("%n %s %n %s", "-----------------", msg.getType());
//        System.out.printf("%n %s %n %s", "-----------------", msg.getType().getDeclaringClass()); // Error:(21, 76) java: java.lang.Enum.getDeclaringClass() is defined in an inaccessible class or interface
  }
}
