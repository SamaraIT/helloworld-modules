package hr.samara.play.support;

import hr.samara.play.renderer.SimpleRenderer;
import org.samara.play.data.Message;

import static org.samara.play.data.type.Type.JSON;

public class RendererSupport {

  private Message message = new Message();

  public void render(String message) {
    this.message.setMessage(message);
    this.message.setType(JSON);
    new SimpleRenderer().renderAsString(this.message);
  }

  public Message getCurrentMessage() {
    return this.message;
  }
}
