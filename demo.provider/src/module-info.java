module demo.provider {
  requires demo.spi;
  provides demo.spi.Greeter with demo.service.impl.GreeterImpl;
}
