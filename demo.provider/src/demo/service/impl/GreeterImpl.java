package demo.service.impl;

import demo.spi.Greeter;

/**
 * If the service provider does not declare a provider() method, then the service provider is instantiated directly, via its provider constructor.
 * A provider constructor is a public constructor with no formal parameters.
 * In this case, the service provider MUST be assignable to the service's interface or class.
 */
public class GreeterImpl implements Greeter {

  @Override
  public void greet() {
    System.out.println("Hello from  " + this.getClass().getSimpleName());
  }
}
